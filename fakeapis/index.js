const get_cart_list = require('./get_cart_list.json');
const get_cart_recommended_goods_list = require('./get_cart_recommended_goods_list.json');
const get_detail_recommended_goods_list = require('./get_detail_recommended_goods_list.json');
const get_goods_detail = require('./get_goods_detail.json');
const insert_cart = require('./insert_cart.json');
const login = require('./login.json');
const logout = require('./logout.json');
const modify_cart_list = require('./modify_cart_list.json');
const search_goods_list = require('./search_goods_list.json');

// Something more

module.exports = () => ({
  get_cart_list: get_cart_list,
  get_cart_recommended_goods_list: get_cart_recommended_goods_list,
  get_detail_recommended_goods_list: get_detail_recommended_goods_list,
  get_goods_detail: get_goods_detail,
  insert_cart: insert_cart,
  login: login,
  logout: logout,
  modify_cart_list: modify_cart_list,
  search_goods_list: search_goods_list
  // Something more
});
